from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.parse

class RequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        parsed_path = urllib.parse.urlparse(self.path)
        name = parsed_path.path[1:]
        if name != 'alice' and name != 'bob':
            self.send_response(404)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.wfile.write("Error: Please provide a valid name in the URL, only 'alice' or 'bob' are accepted".encode())
        else:
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            referrer = self.headers.get("Referer")
            message = f"Hello, my name is {name.capitalize()}"
            if referrer:
                message = f"Hello, I'm {name.capitalize()}, brought to you by {referrer}"
            self.wfile.write(message.encode())


def run(server_class=HTTPServer, handler_class=RequestHandler, port=3000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting server on port {port}...')
    httpd.serve_forever()

run(HTTPServer, RequestHandler)

