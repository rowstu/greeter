# Greeter Application



## Getting started

To run the code, you will need docker and docker-compose installing on your machine. Instructions for this can be found here: (https://docs.docker.com/engine/install/) and (https://docs.docker.com/compose/)

Once docker 

## Download the application

Once docker and docker compose is installed, you can download a zipped archive of the code (https://gitlab.com/rowstu/greeter/-/archive/main/greeter-main.zip). Which you can extract to a working directory on your machine. Or if you have git installed on your machine you can do a git clone of the docker application by clicking clone in the Files Repository and choosing clone with HTTPS. For simplicity here is the link: (https://gitlab.com/rowstu/greeter.git)

## Running the application

Open a terminal window on your machine and issue the following command:

```
$ cd greeter
$ docker-compose up -d
```

This will start the docker application. The application is a simple Python program running on two identical Debian based Docker containers running a Python application that responds to the URL address looking for the name alice or bob. There is an Nginx reverse proxy container running under CentOS that listens on port 8080 using the HTTP protocol and forwards the web request accordingly depending on the first URI that is entered after the server address. If alice is entered, the Nginx reverse proxy will forward the request to the Alice listener. If bob is entered, the request will be forwarded to the Bob listener.

To test this use the following commands. You will need to have the curl application installed and available in your terminal. You can also use your web browser to achieve the same thing.:

```
$ curl http://localhost:8080/alice
Hello, my name is Alice

$ curl http://localhost:8080/bob
Hello, my name is Bob

$ curl http://localhost:8080/fred
<html>
<head><title>404 Not Found</title></head>
<body bgcolor="white">
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.14.1</center>
</body>
</html>

$ curl http://localhost:8080/
<html>
<head><title>404 Not Found</title></head>
<body bgcolor="white">
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.14.1</center>
</body>
</html>

```

As you can see if alice or bob is entered in the first section of the web address after the forward slash you are greeted accordingly. Anything else results in a 404 Not Found error from the reverse proxy server.

Additionally, the Python web application can check the web headers to see how the user was sent to the application. It does this by checking what is called the Referrer header. Again using curl from the command line, you can pass the referrer header for testing purposes.:

```
$ curl --referer www.myfavouritesearchengine.com http://localhost:8080/alice
Hello, I'm Alice, brought to you by www.myfavouritesearchengine.com

```

## Removing the application

If for any reason you wish to remove the application you can use the following command:

```
$ docker-compose down
```

# Greeter Application Methodology and Objectives

The brief had three primary objectives with a few restrictions in place to give the project a realistic feel. Client project applications are rarely simple and usually have many constraints and limitations in place. Therefore the application was delivered based on these limitations rather than best practice.

## Objective 1

The first objective, the echo server was fairly simple in its specification. It allowed you to create an application that simply responded to variables in a web address. Python and Javascript were the easiest tools for me on this. I went with Python simply because I know it better. As this was only a simple demonstration application it did not need anything complicated. If this was a production application, Django or Flask would have been a better choice, but for this I just used the http.server in the Python standard library (https://docs.python.org/3/library/http.server.html)

I started with a basic version of this which had no checking for alice or bob, it simply returned whatever was passed in the URL:

```
from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.parse

class RequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        parsed_path = urllib.parse.urlparse(self.path)
        name = parsed_path.path[1:]

        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        message = f"Hello, my name is {name}."
        self.wfile.write(message.encode())

def run(server_class=HTTPServer, handler_class=RequestHandler, port=3000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print(f'Starting server on port {port}...')
    httpd.serve_forever()

run()
```

I then added a conditional check for 'alice' and returned the 404 response for everything else:

```
if name != 'alice':
            self.send_response(404)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.wfile.write("Error: Please provide a valid name in the URL, only 'alice' is accepted".encode())
        else:
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            message = f"Hello, my name is {name.capitalize()}"
            self.wfile.write(message.encode())
```

After that I amended that condition to also check for 'bob':

```
if name != 'alice' and name != 'bob':
```

Finally, I amended the 200 successful response to amend the return message based on the referer header:

```
referrer = self.headers.get("Referer")
                message = f"Hello, my name is {name.capitalize()}. "
                if referrer:
                    message += f"I was referred by {referrer}."
                self.wfile.write(message.encode())
````

This achieved the second extra credit. The extra credit also slightly amended the wording of the returned message.

When I was satisfied the Python code achieved the requirements, I created a simple Dockerfile as per the requirement to run the code in a Debian container. Ordinarily, such a simple application would have been better run under the latest Python3 based official container. For the first extra credit section the official Python3 image was used. Additionally, using the official Python container would also have considerably reduced the overall size of the greeter container.


## Objective 2

The second objective was a reverse proxy in front of the greeter container. This is a common scenario with web applications. Rather than the user accessing the application server directly, it is best practice to use a proxy server in front of the application server which directs the web request to the correct server. This achieves several things. The primary reasons being security, load balancing and caching. A reverse proxy serves as a security break between the end user and the application code. If the web application had grown organically over time, chances are this may introduce bugs into the code. These bugs are what nefarious users are looking for when they access websites. The load balancing aspect means you have a single point of entry which can direct traffic to the correct destination using a single web hostname. Otherwise administrators would have the difficult job of maintaining multiple DNS address for each application server. This can quickly become unmanageable over time as the number of application servers increase. Finally, the reverse proxy can also act as a cache reducing the load on the application directly.

The objective was simple enough to achieve here with the following Nginx config. 

```
server {
    listen 8080;

    location /alice {
        proxy_pass http://alice:3000;
        proxy_set_header X-Referer $http_referer;
        proxy_set_header Referer $http_referer;
        proxy_pass_request_headers on;
    }
    location /bob {
        proxy_pass http://bob:3000;
        proxy_set_header X-Referer $http_referer;
        proxy_set_header Referer $http_referer;
        proxy_pass_request_headers on;
    }
    location / {
        return 404;
    }
```

The logic is that it listens on port 8080 for /alice and /bob and forwards accordingly. Anything else will return a 404 Not Found error. I decided to implement the name checking functionality here rather than in the application code after the Python code was written. If the application contraints changed over time, then the Python web application would need to be amended as it also checks for alice or bob. If, for example the customer wanted to add more names, it would make sense to remove that check completely and have the code respond with whatever name was entered, then centralising the name check constraint to the proxy config. That would keep the name check logic in one place. The initial reason for having the name check logic in the application was because that was where I started and was able to test without using the proxy server.

## Objective 3

The final primary objective involved pulling the two containers together to form a real web application using a docker-compose.yml file. 

I decided to keep the original Dockerfiles for the greeter and the reverse proxy, as a primary requirement for objective three was to use a separate containers for alice and bob. Using the Dockerfile allowed me to create these separate containers using the same Dockerfile and greeter.py application. It would also allow the application to grow to respond to any additional name requirements that might occur as the application grows were this a real application. This growth would be subject to the Python code changes mentioned in the previous chapter.

If this was a production application, I would ensure that I pinned a specific version of each Docker container. For this demo, I have left it to pull the latest version of the container but it is usually best practice to use a specific version.

Here is the docker compose file. 

```
version: '3'
services:
  reverse-proxy:
    build:
      context: .
      dockerfile: Dockerfile-reverse-proxy
    container_name: reverse-proxy
    ports:
      - 8080:8080
    networks:
      - greeter
  alice:
    build:
      context: .
      dockerfile: Dockerfile-greeter
    container_name: alice
    networks:
      - greeter
  bob:
    build:
      context: .    
      dockerfile: Dockerfile-greeter
    container_name: bob
    networks:
      - greeter

networks:
  greeter:
```

A few things worth mentioning here are that this is where the restriction to only expose the reverse proxy on port 8080 to the outside was achieved by not exposing port 3000 which the greeter application uses to respond to web requests.

Additionally, the use of a named network allowed the use of static names for alice and bob. Without this name resolution inside the container network would not have worked and static IP addresses would have to have been configured to work.

# Extra Credit Sections

## Extra Credit 1

I made the decision to use Gitlab for this. Primarily because I already had a few Gitlab runners configured on a bunch of Raspberry Pi's at home and it made sense to use what I already had and was familiar with. That said, using Github or Bitbucket would also have worked here but using Gitlab was a simple choice and is easy to configure.

The CI/CD pipeline file is as follows:

```
stages:
  - test
  - build
  - deploy

run_tests:
    stage: test
    image: python:3.9-slim-buster
    script:
      - apt-get update && apt-get -y install curl
      - python3 greeter.py &
      - sleep 2
      - curl -s -o /dev/null -w "%{http_code}" http://localhost:3000/alice | grep 200
      - curl -s -o /dev/null -w "%{http_code}" http://localhost:3000/bob | grep 200
      - curl -s -o /dev/null -w "%{http_code}" http://localhost:3000/fred | grep 404
      - curl -s -o /dev/null -w "%{http_code}" http://localhost:3000/ | grep 404

build_images:
    stage: build
    image: tiangolo/docker-with-compose:latest
    services:
      - docker:dind

    before_script:
      - docker info
      - docker-compose --version
      - curl -V

    script:
      - docker-compose build
      - docker-compose up -d
      - sleep 10
      - curl -s -o /dev/null -w "%{http_code}" http://docker:8080/alice | grep 200
      - curl -s -o /dev/null -w "%{http_code}" http://docker:8080/bob | grep 200
      - curl -s -o /dev/null -w "%{http_code}" http://docker:8080/fred | grep 404
      - curl -s -o /dev/null -w "%{http_code}" http://docker:8080/ | grep 404
      - docker-compose down


deploy_to_aws:
    stage: deploy
    when: manual
    before_script:
      - 'command -v ssh-agent >/dev/null'
      - eval $(ssh-agent -s)
      - echo "$AWS_PRIVATE_KEY" | tr -d '\r' | ssh-add -
      - mkdir -p ~/.ssh
      - chmod 700 ~/.ssh
    script:
      - ssh -o "StrictHostKeyChecking no" -i "$AWS_PRIVATE_KEY" $AWS_EC2_USER@$AWS_EC2_IP "cd greeter && docker-compose down"
      - ssh -o "StrictHostKeyChecking no" -i "$AWS_PRIVATE_KEY" $AWS_EC2_USER@$AWS_EC2_IP "git clone https://gitlab.com/rowstu/greeter.git"
      - ssh -o "StrictHostKeyChecking no" -i "$AWS_PRIVATE_KEY" $AWS_EC2_USER@$AWS_EC2_IP "cd greeter && docker-compose pull"
      - ssh -o "StrictHostKeyChecking no" -i "$AWS_PRIVATE_KEY" $AWS_EC2_USER@$AWS_EC2_IP "cd greeter && docker-compose up -d"
    environment:
      name: production
      url: http://$AWS_EC2_IP:8080
    only:
      - main

```

I split the pipeline into three stages. The first was some basic application testing. It simply used a Python3 container, ran the application, and tested the returned message for a 200 or 404. If this was a production grade application, the test would need to be more thorough. Additionally, for a production application it would make sense to do some real unit testing using the standard Python unittest library. But for this demonstration, using curl and grep achieved the requirement.

The second stage was a build stage which used Docker-in-Docker to run the docker-compose file inside of a Docker container. I used a third party container for this simply because the official Docker/Compose container does not have curl installed and I wanted to repeat the tests again in the built application.

The third stage was the deploy stage. I was in two minds on how to deploy the application given that my personal AWS account's bill this month will be costly as I have been using it to study for the AWS Solutions Architect exam. In the end I made the decision not to use the native Cloud Deploy tooling to build the deployment environment. I simply used an EC2 instance which I manually installed docker and docker-compose. 

If this was a production grade application this would not be the approach I would have taken. I would have ensured that for every deployment it would have built the final instance using either Cloud Formation or Terraform to ensure a clean build environment every time and then I would have used DNS in Route53 or a native Application Load Balancer to do a DevOps style deployment, such as blue/green or Canary. That is the reason that as part of the deployment does a docker-compose down and deletes the source directory before cloning the application code and doing a build and up stage. For a demo this works would I would not be confident of this approach in a production deployment.


# Conclusion

I thoroughly enjoyed creating this application; it took a little longer than I had hoped, mainly because I had trouble with the build stage in Gitlab CICD. The curl checks kept failing and the output wasn't clear as to why. I also developed a problem on my Raspberry Pi Gitlab runner which made it difficult to troubleshoot where the actual problem was.

As per extra credit three, the deployed application is live in Docker on an EC2 instance with the URL: http://greeter.rowstu.net/alice or http://greeter.rowstu.net:8080/alice

Feel free to repeat the tests here. To make the site more realistic I have used iptables to forward port 80 to port 8080 so that the user does not have to remember the correct port. This simplifies things for the end user. In a production application it would make sense to have the reverse proxy listen on port 80, or more importantly on port 443 using HTTPS and a valid SSL/TLS certificate but that was beyond the scope of this project.
